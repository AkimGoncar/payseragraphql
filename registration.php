<?php
/**
 * ScandiWeb_PayseraGraphQl
 *
 * @category ScandiWeb
 * @package  ScandiWeb\PayseraGraphQl
 * @author   Akim Goncar <info@scandiweb.com>
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'Scandiweb_PayseraGraphQl',
    __DIR__
);
