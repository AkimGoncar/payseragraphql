import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Paysera from './Paysera.component';
import PayseraQuery from '../../query/Paysera/Paysera.query';
import { fetchQuery } from 'Util/Request';
import { PayseraDispatcher } from '../../store/Paysera';

export const mapDispatchToProps = dispatch => ({
    updatePaysera: (options) => {
        PayseraDispatcher.updatePaysera(dispatch, options);
    }
});

export class PayseraContainer extends ExtensiblePureComponent {
    static propTypes = {
        billingAddress: PropTypes.object.isRequired
    };

    static defaultProps = {
        billingAddress: {}
    };

    state = {
        paysera: []
    }

    componentDidMount() {
        this.getPaysera();
    }

    getPaysera() {
        return fetchQuery([
            PayseraQuery.getPaysera()
        ]).then(
            ({ PayseraPayment }) => {
                this.setState({
                    paysera : PayseraPayment
                });
            }
        );
    }

    containerFunctions = {
        selectPayseraMethod: this.selectPayserMethod.bind(this)
    }

    selectPayserMethod(id) {
        const { updatePaysera } = this.props;

        updatePaysera({ paysera: id });
    }

    render() {
        return (
            <Paysera
              { ...this.props }
              { ...this.containerFunctions }
              { ...this.state }
            />
        );
    }
}

export  default connect(null, mapDispatchToProps)
(middleware(PayseraContainer, 'Component/Paysera/Container'));
