/**
 * ScandiPWA - Progressive Web App for Magento
 *
 * Copyright © Scandiweb, Inc. All rights reserved.
 * See LICENSE for license details.
 *
 * @license OSL-3.0 (Open Software License ("OSL") v. 3.0)
 * @package scandipwa/base-theme
 * @link https://github.com/scandipwa/base-theme
 */
import PropTypes from 'prop-types';
import Field from 'Component/Field';

import './Paysera.style';

export class Paysera extends ExtensiblePureComponent {
    static propTypes = {
        paysera: PropTypes.array.isRequired,
        billingAddress: PropTypes.object.isRequired,
        selectPayseraMethod: PropTypes.func.isRequired
    };

    state= {
        isLoading: true,
        currentCountryId: 'lt', // Placeholder value
        selectedMethod: ''
    }

    componentDidMount() {
        const { billingAddress: { country_id } } = this.props;
        this.setState({
            currentCountryId: country_id.toLowerCase()
        });
    }

    onChange = (value) => {
        this.setState({
            currentCountryId: value
        });
    };

    onClick = (id) => {
        const { selectPayseraMethod } = this.props;
        this.setState({
            selectedMethod: id
        }, () => selectPayseraMethod(id));
    };

    renderCountrySelect() {
        const { currentCountryId } = this.state;
        const { paysera } = this.props;
        const selectOptions = paysera.map(
            ({id, title}) => ({id, label: title, value: id}));

        return (
            <Field
              id="CountrySelect"
              name="CountrySelect"
              type="select"
              placeholder={ __('Select Country')}
              mix={ { block: 'Paysera', elem: 'Select' } }
              selectOptions={ selectOptions }
              value={ currentCountryId }
              onChange={ this.onChange }
            />
        );
    }

    renderMethod(id, title, logo) {
        const { selectedMethod } = this.state;

        const isSelected = id === selectedMethod;

        return (
            <div block="Paysera" elem="Method" key={ id }>
                <div block="Paysera" elem="MethodButton">
                    <button
                    block="Paysera"
                    mods={ { isSelected } }
                    elem="Button"
                    value={ id }
                    onClick={ () => this.onClick(id) }
                    type="button"
                    >
                        <img src={ logo } alt={ title } />
                    </button>
                </div>
            </div>
        );
    }

    renderGroup(id, title, payseraPaymentMethods) {
        return (
            <div block="Paysera" elem="Group" key={ id }>
                <p block="Paysera" elem="GroupTitle">
                    { title }
                </p>
                <div block="Paysera" elem="Methods">
                    { payseraPaymentMethods.map(
                        ({ id, title, logo }) => this.renderMethod(id, title, logo))}
                </div>
            </div>
        );
    }

    renderGroups() {
        const { paysera } = this.props;
        const { currentCountryId } = this.state;

        if (!paysera.length) {
            return null;
        } else {
            const { payseraGroups } = paysera.find(({ id }) => id === currentCountryId);

            return (
                payseraGroups.map(
                    ({ id, title, payseraPaymentMethods }) =>
                    this.renderGroup(id, title, payseraPaymentMethods))
            );
        }
    }

    render() {
        return (
            <div block="Paysera">
                { this.renderCountrySelect() }
                { this.renderGroups() }
            </div>
        );
    }
}

export default middleware(Paysera, 'Component/Paysera/Component');
