/**
 * ScandiPWA - Progressive Web App for Magento
 *
 * Copyright © Scandiweb, Inc. All rights reserved.
 * See LICENSE for license details.
 *
 * @license OSL-3.0 (Open Software License ("OSL") v. 3.0)
 * @package scandipwa/base-theme
 * @link https://github.com/scandipwa/base-theme
 */

import { Field } from 'Util/Query';

export class PayseraQuery extends ExtensibleClass {
    getPaysera() {
        return new Field('PayseraPayment')
            .addFieldList(this.getPayseraCountry())
            .setAlias('PayseraPayment');
    }

    getPayseraCountry() {
        return [
            'id',
            'title',
            this.getPayseraGroup()
        ]
    }

    getPayseraGroup() {
        return new Field('payseraGroups')
            .addFieldList([
                'id',
                'title',
                this.getPayseraMethods()
            ])
    }

    getPayseraMethods() {
        return new Field('payseraPaymentMethods')
            .addFieldList([
                'id',
                'title',
                'logo'
            ])
    }

    setCheckoutSessionPayseraStatus(id) {
        return new Field('PayseraStatus')
            .addArgument('id', 'String', id)
            .setAlias('PayseraStatus');
    }
}

export default new (middleware(PayseraQuery, 'Query/Paysera'))();
