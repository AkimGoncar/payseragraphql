class CheckoutComponentPlugin extends ExtensibleClass {
    componentDidMount = (args, callback, instance) => {
        this.renderRedirectSuccess(instance);
        callback.apply(instance, args);
    }

    renderRedirectSuccess(context) {
        const { redirectSuccess } = context.props;
        const getOrderId = (new URLSearchParams(window.location.search)).get('orderId');
        if (!getOrderId) {
            return
        } else {
            const idOrder = getOrderId.replace('/', '');
            redirectSuccess(idOrder);
        }
    }
}

const { componentDidMount } = new CheckoutComponentPlugin();

const config = {
    'Route/Checkout/Component': {
        'member-function': {
            componentDidMount: [
                {
                    position: 113,
                    implementation: componentDidMount
                }
            ]
        }
    }
}

export default config;
