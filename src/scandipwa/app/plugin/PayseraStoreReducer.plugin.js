import { PayseraReducer } from '../store/Paysera';

class PayseraReducerPlugin extends ExtensibleClass {
    getReducer = (args, callback, instance) => ({
        ...callback.apply(instance, args),
        PayseraReducer
    })
}

const { getReducer } = new PayseraReducerPlugin();

const config = {
    'Store/Index/getReducers': {
        'function': [
            {
                position: 93,
                implementation: getReducer
            }
        ]
    },
}

export default config;
