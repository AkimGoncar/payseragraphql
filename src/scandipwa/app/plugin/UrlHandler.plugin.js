class UrlHandlerPlugin extends ExtensibleClass {
    getBypassCacheHosts = (args, callback, instance) => ([
        '(?!^.*paysera)', // Paysera Payment Method
        ...callback.apply(instance, args),
    ])
}

const { getBypassCacheHosts } = new UrlHandlerPlugin();

const config = {
    'SW/Handler/UrlHandler/getBypassCacheHosts': {
        'function': [
            {
                position: 93,
                implementation: getBypassCacheHosts
            }
        ]
    }
}

export default config;
