import axios from 'axios';
import { DETAILS_STEP } from 'Route/Checkout/Checkout.component';
import { fetchQuery, fetchMutation } from 'Util/Request';
import CheckoutQuery from 'Query/Checkout.query';
import { isSignedIn } from 'Util/Auth';
import BrowserDatabase from 'Util/BrowserDatabase';
import { PAYSERA_CODE } from '../util/PayseraConfig/config';
import PayseraQuery from '../query/Paysera/Paysera.query';

export const PAYMENT_TOTALS = 'PAYMENT_TOTALS';

class CheckoutContainerPlugin extends ExtensibleClass {
    containerFunctions = (originalMember, instance) => {
        return {
            ...originalMember,
            redirectSuccess: this.redirectSuccess.bind(instance)
        }
    }

    redirectSuccess(orderID) {
        this.setState({
            orderID,
            checkoutStep: DETAILS_STEP,
            isLoading: false
        });
    }

    savePaymentMethodAndPlaceOrder = async (args, callback, instance) => {
        const [paymentInformation] = args;
        const { paymentMethod: { code, additional_data } } = paymentInformation;

        if (code !== PAYSERA_CODE) {
            callback.apply(instance, args);
            return;
        }
        const guest_cart_id = !isSignedIn() ? instance._getGuestCartId() : '';

        try {
            await fetchMutation(CheckoutQuery.getSetPaymentMethodOnCartMutation({
                guest_cart_id,
                payment_method: {
                    code, [code]: additional_data
                }
            }));

            const orderData = await fetchMutation(CheckoutQuery.getPlaceOrderMutation(guest_cart_id));
            const { placeOrder: { order: { order_id } } } = orderData;

            instance.setDetailsStep(order_id, code);
        } catch (e) {
            instance._handleError(e);
        }
    }

    setDetailsStep = (args, callback, instance) => {
        const { paysera_method_code, resetCart } = instance.props;
        const [orderID, code] = args;

        if (code !== PAYSERA_CODE) {
            callback.apply(instance, args);
            return;
        }
        BrowserDatabase.deleteItem(PAYMENT_TOTALS);
        resetCart();
        instance.setState({
            isLoading: true,
            paymentTotals: {},
            orderID
        });

        this.getPaysera(instance, paysera_method_code);
    }

    async getPaysera(context, id) {
        const { orderID } = context.state;

        await this.setPayseraMethod(id);

        const config = {
            headers: {
                'Pragma': 'no-cache',
                'Cache-Control': 'no-cache'
            },
            params: {
                orderId: orderID
            }
        };

        axios.get('/paysera', config)
            .then(res => {
                this.redirect(res.data)
            })
            .catch(console.error())
    }

    setPayseraMethod(id) {
        return fetchQuery([
            PayseraQuery.setCheckoutSessionPayseraStatus(id)
        ]);
    }

    redirect(response) {
        const { url } = response;

        if (url !== undefined) {
            window.location.replace(url);
        }
    }
}

class CheckoutStatePlugin extends ExtensibleClass {
    mapStateToProps = (args, callback, instance) => {
        const [state] = args;

        return {
            ...callback.apply(instance, args),
            paysera_method_code: state.PayseraReducer.paysera
        }
    }
}

const {
    savePaymentMethodAndPlaceOrder,
    setDetailsStep
} = new CheckoutContainerPlugin();

const { mapStateToProps } = new CheckoutStatePlugin();


const config = {
    'Route/Checkout/Container/mapStateToProps': {
        'function': [
                {
                    position: 91,
                    implementation: mapStateToProps
                }
            ]
        },
        'Route/Checkout/Container': {
            'member-function': {
                savePaymentMethodAndPlaceOrder: [
                    {
                        position: 100,
                        implementation: savePaymentMethodAndPlaceOrder
                    }
                ],
                setDetailsStep: [
                    {
                        position: 101,
                        implementation: setDetailsStep
                    }
                ]
            }
        }
};


export default config;
