/*
 * ScandiPWA - Progressive Web App for Magento
 *
 * Copyright © Scandiweb, Inc. All rights reserved.
 * See LICENSE for license details.
 *
 * @license OSL-3.0 (Open Software License ("OSL") v. 3.0)
 * @package scandiweb/paytrail-scandipwa
 * @author    Akims Goncars <info@scandiweb.com>
 */

import CheckoutPaymentsPlugin from './CheckoutPayments.plugin';
import UrlHandlerPlugin from './UrlHandler.plugin';
import PayseraReducerPlugin from './PayseraStoreReducer.plugin';
import CheckoutContainerPlugin from './Checkout.container.plugin';
import CheckoutStatePlugin from './Checkout.container.plugin';
import CheckoutComponentPlugin from './Checkout.component.plugin';

export default {
    ...CheckoutPaymentsPlugin,
    ...UrlHandlerPlugin,
    ...PayseraReducerPlugin,
    ...CheckoutContainerPlugin,
    ...CheckoutStatePlugin,
    ...CheckoutComponentPlugin
}
