import Paysera from '../component/Paysera';

export const PAYSERA = 'paysera';

class CheckoutPaymentsPlugin extends ExtensibleClass {
    renderPaysera() {
        const { billingAddress } = this.props;

        return (
            <Paysera
              billingAddress={ billingAddress }
            />
        );
    }

    paymentRenderMap = (originalMember, instance) => {
        return {
            ...originalMember,
            [PAYSERA]: this.renderPaysera.bind(instance)
        }
    }
}

const {
    paymentRenderMap
} = new CheckoutPaymentsPlugin();

const config = {
    'Component/CheckoutPayments/Component': {
        'member-property': {
            'paymentRenderMap': [
                {
                    position: 98,
                    implementation: paymentRenderMap
                }
            ]
        }
    }
};

export default config;
