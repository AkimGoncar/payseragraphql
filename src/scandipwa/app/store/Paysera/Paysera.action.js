/**
 * ScandiPWA - Progressive Web App for Magento
 *
 * Copyright © Scandiweb, Inc. All rights reserved.
 * See LICENSE for license details.
 *
 * @license OSL-3.0 (Open Software License ("OSL") v. 3.0)
 * @package scandipwa/base-theme
 * @link https://github.com/scandipwa/base-theme
 */

export const UPDATE_PAYSERA = 'UPDATE_PAYSERA';

/**
 * Update router to show Paysera page
 * @param  {Boolean} Paysera New Paysera value
 * @return {void}
 */
export const updatePaysera = paysera => ({
    type: UPDATE_PAYSERA,
    paysera
});
