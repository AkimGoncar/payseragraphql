/**
 * ScandiPWA - Progressive Web App for Magento
 *
 * Copyright © Scandiweb, Inc. All rights reserved.
 * See LICENSE for license details.
 *
 * @license OSL-3.0 (Open Software License ("OSL") v. 3.0)
 * @package scandipwa/base-theme
 * @link https://github.com/scandipwa/base-theme
 */

import {
    UPDATE_PAYSERA
} from './Paysera.action';

export const initialState = {
    paysera: 'paysera'
};

export const PayseraReducer = (state = initialState, action) => {
    const { paysera } = action;

    switch (action.type) {
    case UPDATE_PAYSERA:
        return { ...state, paysera };

    default:
        return state;
    }
};

export default PayseraReducer;
