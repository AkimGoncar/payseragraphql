/**
 * ScandiPWA - Progressive Web App for Magento
 *
 * Copyright © Scandiweb, Inc. All rights reserved.
 * See LICENSE for license details.
 *
 * @license OSL-3.0 (Open Software License ("OSL") v. 3.0)
 * @package scandipwa/base-theme
 * @link https://github.com/scandipwa/base-theme
 */

import { updatePaysera } from './';
/**
 * Paysera Dispatcher
 * @class PayseraDispatcher
 */
export class PayseraDispatcher extends ExtensibleClass {
    updatePaysera(dispatch, options) {
        const { paysera } = options;
        dispatch(updatePaysera(paysera));
    }
}

export default new (middleware(PayseraDispatcher, 'Store/Paysera/Dispatcher'))();
