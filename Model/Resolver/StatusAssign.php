<?php
/**
 * ScandiWeb_PayseraGraphQl
 *
 * @category ScandiWeb
 * @package  ScandiWeb\PayseraGraphQl
 * @author   Akim Goncar <info@scandiweb.com>
 */
declare(strict_types=1);

namespace Scandiweb\PayseraGraphQl\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Paysera\Magento2Paysera\Helper\Data;
use Psr\Log\LoggerInterface;

class StatusAssign implements ResolverInterface
{

    const PAYSERA_TYPE = 'paysera_payment_type';

    /**
     * @var
     */
    protected $_helper;

    /**
     * @var
     */
    protected $logger;

    /**
     * Fetches the data from persistence models and format it according to the GraphQL schema.
     *
     * @param Field            $field
     * @param ContextInterface $context
     * @param ResolveInfo      $info
     * @param array|null       $value
     * @param array|null       $args
     *
     * @return mixed|Value
     * @throws \Exception
     */

    public function __construct(
        Data $helper,
        LoggerInterface $logger
    )
    {
        $this->_helper = $helper;
        $this->logger = $logger;
    }

    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {

        try {
            $paymentType = $args['id'];
            $this->setSelectedData(self::PAYSERA_TYPE, $paymentType);
            $response = "Success";
        } catch(\Magento\Framework\Exception\LocalizedException $e)  {
            $this->logger->critical('Error message', ['exception' => $e]);
            $response = 'Fail';
        }

        return $response;
    }

    protected function setSelectedData($key, $value)
    {
        return $this->_helper->setSessionData($key, $value);
    }
}
