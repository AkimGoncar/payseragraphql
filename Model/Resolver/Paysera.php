<?php
/**
 * ScandiWeb_PayseraGraphQl
 *
 * @category ScandiWeb
 * @package  ScandiWeb\PayseraGraphQl
 * @author   Akim Goncar <info@scandiweb.com>
 */
declare(strict_types=1);

namespace Scandiweb\PayseraGraphQl\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Payment\Model\Config;
use Paysera\Magento2Paysera\Model\PayseraConfigProvider;
use Magento\Checkout\Model\ConfigProviderInterface;
use Paysera\Magento2Paysera\Helper\Data;

class Paysera extends PayseraConfigProvider implements ResolverInterface
{
    /**
     * @var $_helper
     */
    protected $_helper;

    /**
     * @var $paymentConfig
     */
    protected $paymentConfig;
    /**
     * Fetches the data from persistence models and format it according to the GraphQL schema.
     *
     * @param Field            $field
     * @param ContextInterface $context
     * @param ResolveInfo      $info
     * @param array|null       $value
     * @param array|null       $args
     *
     * @return mixed|Value
     * @throws \Exception
     */

    public function __construct(
        Config $paymentConfig,
        Data $helper
    )
    {
        parent::__construct($helper);
        $this->paymentConfig = $paymentConfig;
    }

    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {

        $data = [];
        $configProjectId = $this->getPluginConfig(self::PROJECT_ID);

        $payseraCountries = $this->getPayseraCountries(
            $this->getValidProject($configProjectId),
            $this->getOrderCurrency(),
            $this->getStoreLang()
        );

        $countries = $this->getCountriesList($payseraCountries);

        foreach($countries as $country) {
            $paysera_data = [
                'id' => $country['code'],
                "title" => $country['title'],
                'payseraGroups' => []
            ];

            if(is_array($country['groups'])) {
                $payseraGroups = $country['groups'];

                foreach($payseraGroups as $payseraGroup){
                    $group = [
                        'id' => $payseraGroup->getKey(),
                        'title' =>$payseraGroup->getTitle(),
                        'payseraPaymentMethods' => []
                    ];

                    $payseraMethods = $payseraGroup->getPaymentMethods();

                    foreach($payseraMethods as $payseraMethod){
                        $method = [
                            'id' => $payseraMethod->getKey(),
                            'title' => $payseraMethod->getTitle(),
                            'logo' => $payseraMethod->getLogoUrl()
                        ];
                        array_push($group['payseraPaymentMethods'], $method);
                    }
                    array_push($paysera_data['payseraGroups'], $group);
                }
                array_push($data, $paysera_data);
            }
        }
        return $data;
    }
}
